//RANDOM DRINK

//Capture elements

var random = document.getElementById("random");
var title = document.getElementById("title");
var image = document.getElementById("image");
var recipe = document.getElementById("recipe");
var category = document.getElementById("category");

//Event -> Fetch API random -> replace data //

random.addEventListener("click", function () {
    fetch('https://www.thecocktaildb.com/api/json/v1/1/random.php')
        .then(function (response) {
            return response.json();
        })
        .then(function (data) {
            title.innerHTML = data.drinks[0].strDrink;
            image.src = data.drinks[0].strDrinkThumb;
            recipe.innerHTML = data.drinks[0].strInstructions;
            category.innerHTML = data.drinks[0].strCategory;
        })
        .catch(function (err) {
            console.error(err);
        });

})

//SEARCH

var searchIButton = document.getElementById("searchIButton");
var search = document.getElementById("search");
var cardIngredient = document.getElementsByClassName("cardIngredient");
var description = document.getElementById("description");

searchIButton.addEventListener("click", function () {
    var ingredient = search.value
    if (ingredient != '') {
        fetch("https://www.thecocktaildb.com/api/json/v1/1/search.php?i=" + ingredient)
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
                cardIngredient[0].classList.remove("d-none");

                var name = document.getElementById("name");
                name.innerHTML = data.ingredients[0].strIngredient;

                var type = document.getElementById("type");
                type.innerHTML = "Type: " + data.ingredients[0].strType;

                var description = document.getElementById("description");
                description.innerHTML = data.ingredients[0].strDescription;

                var rate = document.getElementById("rate");
                if (data.ingredients[0].strAlcohol != null) {
                    return rate.innerHTML = "Alcoholic Drink";
                } else {
                    return rate.innerHTML = "Non-alcoholic Drink";
                }

            }).catch(function (err) {
                console.error(err);
            });
    }
})


